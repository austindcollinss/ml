import os, struct
from array import array as pyarray
import numpy as np

class DataGrabber:

    def __init__(self):
        pass

    def load_mnist(self, dataset="training", digits=range(10), path="data"):

        if dataset == "training":
            fname_image = os.path.join(path, 'train-images-idx3-ubyte')
            fname_label = os.path.join(path, 'train-labels-idx1-ubyte')
        elif dataset == "testing":
            fname_image = os.path.join(path, 't10k-images-idx3-ubyte')
            fname_label = os.path.join(path, 't10k-labels-idx1-ubyte')
        else:
            raise ValueError("dataset must be 'testing' or 'training'")

        # Extract labels
        file_lbl = open(fname_label, 'rb')
        magic_nr, size = struct.unpack(">II", file_lbl.read(8))
        lbl = np.fromstring(file_lbl.read(), np.uint8)
        file_lbl.close()

        # Extract Images
        file_img = open(fname_image, 'rb')
        magic_nr, size, rows, cols = struct.unpack(">IIII", file_img.read(16))
        img = np.fromstring(file_img.read(), np.uint8)
        file_img.close()

        # Indices of digits in digits input
        ind = [k for k in range(size) if lbl[k] in digits]
        n = len(ind)

        images = np.zeros((n, rows, cols))
        labels = np.zeros(n)
        for i in range(len(ind)):
            images[i] = np.array(img[ind[i] * rows * cols: (ind[i] + 1) * rows * cols]).reshape((rows, cols))
            labels[i] = lbl[ind[i]]

        return images, labels


