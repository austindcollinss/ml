import numpy as np
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data


from ots_learners.Learner import Learner

class SoftmaxRegression(Learner):

    def __init__(self):
        self.sess = tf.InteractiveSession()
        pass

    def fit(self, X, y):
        digits = 10
        self.graph_input = tf.placeholder(tf.float32, shape=[None, 784])
        self.graph_output = tf.placeholder(tf.float32, shape=[None, digits])
        W = tf.Variable(tf.zeros([784, digits]))
        b = tf.Variable(tf.zeros([digits]))
        self.sess.run(tf.initialize_all_variables())

        self.output = tf.nn.softmax(tf.matmul(self.graph_input, W) + b)
        cross_entropy = tf.reduce_mean(-tf.reduce_sum(self.graph_output * tf.log(self.output), reduction_indices=[1]))
        train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)

        for i in range(200):
            train_step.run(feed_dict={self.graph_input: X, self.graph_output: y})


    def predict(self, X):
        self.sess.run(self.output, feed_dict={self.graph_input: X})
        return tf.argmax(self.output,1).eval(feed_dict={self.graph_input: X})


    def to_tf_format(self, y, digits):
        y_out = np.zeros((len(y), digits))
        for i in range(len(y)):
            y_out[i,int(y[i])] = 1
        return y_out.astype(int)
