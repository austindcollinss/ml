import numpy as np
import matplotlib.pyplot as plt
import time

import DataGrabber
from ots_learners.ots_SVM import ots_SVM
from ots_learners.ots_Linreg import ots_Linreg
# from ots_learners.ots_Logreg import ots_Logreg
#from ots_learners.ots_Deep_nn import ots_Deep_nn
# from ots_learners.Autoencoder import Autoencoder
from tensor_flow.softmax_regression import SoftmaxRegression
from tensor_flow.MultilayerPerceptron import MultilayerPerceptron
from tensor_flow.Autoencoder import Autoencoder
from tensor_flow.RecurrentNN import RecurrentNN
from tensor_flow.VAE import VariationalAutoencoder
from tensor_flow.VAE2 import VariationalAutoencoder2


def main():

    # Import datals -l
    dg = DataGrabber.DataGrabber()
    digits = range(10)
    #digits = [0,1]
    training_images, training_labels = dg.load_mnist("training", digits)
    testing_images, testing_labels   = dg.load_mnist("testing",  digits)
    training_labels_mat = to_tf_format(training_labels, len(digits))
    testing_labels_mat = to_tf_format(testing_labels, len(digits))

    # Display images in mnist data
    #plt.imshow(images.mean(axis=0), cmap = plt.cm.gray)
    # plt.show()

    # Vectorized
    vector_training_images = vectorize_data(training_images)
    vector_testing_images  = vectorize_data(testing_images)

    ##### Off the shelf algorithm section #####
    print "Learning with", vector_training_images.shape[0], "samples",
    print "and", vector_training_images.shape[1], "features"

    # print
    # print "Running tensorflow NN..."
    # nn_tensor = SoftmaxRegression()
    # nn_tensor.fit(vector_training_images, training_labels)
    # y_nn_tf = nn_tensor.predict(vector_testing_images)
    # print "TF NN error fraction:", compute_percent_error(testing_labels, y_nn_tf)

    # print
    # print "Running Multilayer Perceptron..."
    # nn_tensor = MultilayerPerceptron()
    # nn_tensor.fit(vector_training_images, training_labels_mat)
    # y_mp_tf = nn_tensor.predict(vector_testing_images)
    # print "MP error fraction:", compute_percent_error(testing_labels, y_mp_tf)
    #
    # print
    # print "Running Recurrent NN..."
    # rnn = RecurrentNN()
    # tic = time.time()
    # rnn.fit(vector_training_images, training_labels_mat)
    # toc = time.time()
    # print "Training time:", toc - tic
    # tic = time.time()
    # y_rnn = rnn.predict(vector_testing_images)
    # toc = time.time()
    # print "Prediction time:", toc - tic
    # print "RNN error fraction:", compute_percent_error(testing_labels, y_rnn)

    print
    print "Running Autoencoder..."
    #\vector_testing_images = vector_testing_images / 255.0
    #vector_training_images = vector_training_images / 255.0
    #ae = Autoencoder(n_hidden=500, n_bottleneck=36)
    ae = VariationalAutoencoder2()
    tic = time.time()
    ae.fit(vector_training_images)
    toc = time.time()
    print "Autoencoder fitting time:", toc - tic
    print ae.get_features(vector_testing_images)
    y_pred = ae.predict(vector_testing_images[:200,:])
    #features = ae.get_features(vector_testing_images)
    #max_activations = ae.max_activations(vector_testing_images)

    for i in range(40):
        fig, ax = plt.subplots(2)
        ax[0].imshow(vector_testing_images[i,:].reshape(28,28), cmap=plt.gray())
        #ax[1].imshow(max_activations[:, i].reshape(28, 28), cmap=plt.gray())
        ax[1].imshow(y_pred[i,:].reshape(28, 28), cmap=plt.gray())
        plt.show()



    # # Linear Regression
    # print
    # print "Running linear regression with rounding..."
    # tic = time.time()
    # linreg = ots_Linreg()
    # linreg.fit(vector_training_images, training_labels)
    # linreg_y_hat = linreg.predict(vector_testing_images)
    # toc=time.time()
    # linreg_errors = compute_percent_error(testing_labels, linreg_y_hat)
    # print "Linear regression run time:", toc-tic
    # print "Linear regression:", linreg_errors, "fraction incorrect"
    #
    # # Linear Regression
    # print
    # print "Running linear regression with rounding and feature data..."
    # tic = time.time()
    # linreg = ots_Linreg()
    # training_features = ae.get_features(vector_training_images)
    # training_features = np.hstack([vector_training_images, training_features])
    # linreg.fit(training_features, training_labels)
    # test_features = ae.get_features(vector_testing_images)
    # test_features = np.hstack([vector_testing_images, test_features])
    # linreg_y_hat = linreg.predict(test_features)
    # toc = time.time()
    # linreg_errors = compute_percent_error(testing_labels, linreg_y_hat)
    # print "Linear regression with all features run time:", toc - tic
    # print "Linear regression with all features:", linreg_errors, "fraction incorrect"
    #
    # # Linear Regression
    # print
    # print "Running linear regression with rounding and feature data..."
    # tic = time.time()
    # linreg = ots_Linreg()
    # training_features = ae.get_features(vector_training_images)
    # linreg.fit(training_features, training_labels)
    # test_features = ae.get_features(vector_testing_images)
    # linreg_y_hat = linreg.predict(test_features)
    # toc = time.time()
    # linreg_errors = compute_percent_error(testing_labels, linreg_y_hat)
    # print "Linear regression with extracted features only run time:", toc - tic
    # print "Linear regression with extracted features only:", linreg_errors, "fraction incorrect"

    # # Logistic Regression
    # print
    # print "Running Logistic Regression..."
    # tic = time.time()
    # logreg = ots_Logreg()
    # logreg.fit(vector_training_images, training_labels)
    # logreg_y_hat = logreg.predict(vector_testing_images)
    # toc = time.time()
    # logreg_errors = compute_percent_error(testing_labels, logreg_y_hat)
    # print "Logistic regression run time:", toc - tic
    # print "Logistic regression:", logreg_errors, "fraction incorrect"
    #
    # SVM
    # print
    # print "Running SVM..."
    # tic=time.time()
    # svm = ots_SVM()
    # svm.fit(vector_training_images, training_labels)
    # svm_y_hat = svm.predict(vector_testing_images)
    # toc=time.time()
    # svm_error = compute_percent_error(testing_labels, svm_y_hat)
    # print "SVM running time:", toc - tic
    # print "SVM:", svm_error, "fraction incorrect"
    #
    # # Deep NN
    # print
    # print "Running Deep NN..."
    # tic = time.time()
    # deepnn = ots_Deep_nn()
    # deepnn.fit(vector_training_images, training_labels)
    # deepnn_y_hat = deepnn.predict(vector_testing_images)
    # toc = time.time()
    # deepnn_error = compute_percent_error(testing_labels, deepnn_y_hat)
    # print "Deep NN running time:", toc - tic
    # print "Deep NN:", deepnn_error, "fraction incorrect"

    # X_train = training_images.reshape((-1, 1, 28, 28))
    # X_test = testing_images.reshape((-1, 1, 28, 28))
    # y_train = training_labels.astype(np.uint8)
    # y_test = testing_labels.astype(np.uint8)
    #
    #
    # print
    # print "Running Autoencoder..."
    # auto = Autoencoder()
    # auto.fit(X_train, y_train)
    # pred = auto.predict(X_test)
    # auto.confusion_matrix(y_test, pred)


def to_tf_format(y, digits):
    y_out = np.zeros((len(y), digits))
    for i in range(len(y)):
        y_out[i, int(y[i])] = 1
    return y_out.astype(int)


def vectorize_data(arr):
    n = arr.shape[0]
    return arr.reshape(n, -1)

def compute_percent_error(y, y_hat):
    return ((y != y_hat).sum() / float(len(y)))





if __name__=="__main__":
    main()