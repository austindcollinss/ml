import numpy as np
import tensorflow as tf

class MultilayerPerceptron:

    def __init__(self):
        pass

    def fit(self, X_train, y_train):
        # Parameters
        learning_rate = 0.01
        training_epochs = 300
        batch_size = 100
        display_step = 1

        # Network Parameters
        layer_size = {'n_hidden_1' : 512,
                      'n_hidden_2' : 256,
                      'n_input'    : 784,
                      'n_classes'  : 10}

        # Network input / ouput placeholders
        self.x = tf.placeholder(tf.float32, [None, layer_size['n_input']])
        self.y = tf.placeholder(tf.float32, [None, layer_size['n_classes']])

        # Network structure, cost, and optmization method
        self.mp = self.build_network(self.x, layer_size)
        cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(self.mp, self.y))
        optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)

        # Create session
        init = tf.initialize_all_variables()
        self.sess = tf.Session()
        self.sess.run(init)

        # Training loop
        for epoch in range(training_epochs):
            print "Epoch " + str(epoch) + "..."
            avg_cost = 0
            total_batch = int(X_train.shape[0] / float(batch_size))
            for i in range(total_batch):
                batch_x = X_train[batch_size*i:batch_size*(i+1),:]
                batch_y = y_train[batch_size*i:batch_size*(i+1),:]
                _, c = self.sess.run([optimizer, cost], feed_dict={self.x: batch_x, self.y: batch_y})
                avg_cost += c / total_batch
                #if epoch % display_step == 0:
                    #print "Epoch:", '%04d' % (epoch + 1), "cost={:.9f}".format(avg_cost)
        print "Optimization Finished!"

    def predict(self, X_test):
        return self.sess.run(tf.argmax(self.mp, 1), feed_dict={self.x: X_test})

    def build_network(self, x, layer_size):
        weights = { 'w1' : tf.Variable(tf.random_normal([layer_size['n_input'], layer_size['n_hidden_1']])),
                    'w2': tf.Variable(tf.random_normal([layer_size['n_hidden_1'], layer_size['n_hidden_2']])),
                    'out': tf.Variable(tf.random_normal([layer_size['n_hidden_2'], layer_size['n_classes']]))
                    }
        biases  = {'b1': tf.Variable(tf.random_normal([layer_size['n_hidden_1']])),
                   'b2': tf.Variable(tf.random_normal([layer_size['n_hidden_2']])),
                   'out': tf.Variable(tf.random_normal([layer_size['n_classes']]))
                   }
        mp = self.multilayer_perceptron(x, weights, biases)
        return mp

    def multilayer_perceptron(self, x, weights, biases):
        layer_1 = tf.add(tf.matmul(x, weights['w1']), biases['b1'])
        layer_1 = tf.nn.sigmoid(layer_1)
        layer_2 = tf.add(tf.matmul(layer_1, weights['w2']), biases['b2'])
        layer_2 = tf.nn.sigmoid(layer_2)
        out_layer = tf.add(tf.matmul(layer_2, weights['out']), biases['out'])
        return out_layer
