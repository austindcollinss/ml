from nolearn.dbn import DBN

class ots_Deep_nn:

    def __init__(self):
        self.clf = DBN()

    def fit(self, X, y):
        self.clf.fit(X, y)

    def predict(self, X):
        return self.clf.predict(X)
