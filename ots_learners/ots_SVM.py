from sklearn import svm


class ots_SVM:

    def __init__(self):
        self.clf = svm.SVC()

    def fit(self, X, y):
        print "Fitting SVM..."
        self.clf.fit(X,y)

    def predict(self, X):
        print "Predicting SVM..."
        return self.clf.predict(X)
