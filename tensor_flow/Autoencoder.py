import numpy as np
import tensorflow as tf

class Autoencoder:

    def __init__(self, n_hidden=400, n_bottleneck=400):
        self.n_hidden = n_hidden
        self.n_bottleneck = n_bottleneck

    def fit(self, X):
        # Parameters
        learning_rate = 0.01
        training_epochs = 10
        batch_size = 250
        display_step = 1

        # Network Parameters
        layer_size = {'n_hidden'     : self.n_hidden,
                      'n_bottleneck' : self.n_bottleneck,
                      'n_input'      : 784,
                      'n_output'     : 784}

        # Network input / ouput placeholders
        self.X = tf.placeholder(tf.float32, [None, layer_size['n_input']])

        # Network structure, cost, and optmization method
        self.enc, self.dec = self.build_network(self.X, layer_size)
        y_pred = self.dec
        y_true = self.X
        cost = tf.reduce_mean(tf.pow(y_true - y_pred,2)) + self.l1_reg(self.weights['enc_w1'] + self.biases['enc_b1'])
        #cost = self.loss_x_entropy(y_pred, y_true)
        optimizer = tf.train.RMSPropOptimizer(learning_rate).minimize(cost)

        # Create session
        init = tf.initialize_all_variables()
        self.sess = tf.Session()
        self.sess.run(init)

        # Training loop
        for epoch in range(training_epochs):
            print "Epoch " + str(epoch) + ",",
            avg_cost = 0
            total_batch = int(X.shape[0] / float(batch_size))
            for i in range(total_batch):
                batch_x = X[batch_size*i:batch_size*(i+1),:]
                _, c = self.sess.run([optimizer, cost], feed_dict={self.X: batch_x})
                avg_cost += c / total_batch
            print "avg_cost:", avg_cost
        print "Optimization Finished!"

    def predict(self, X):
        return self.sess.run(self.dec, feed_dict={self.X: X})

    def get_features(self, X):
        return self.sess.run(self.enc, feed_dict={self.X: X})

    def build_network(self, x, layer_size):
        self.weights = { 'enc_w1' : tf.Variable(tf.random_normal([layer_size['n_input'], layer_size['n_hidden']])),
                    'enc_w2': tf.Variable(tf.random_normal([layer_size['n_hidden'], layer_size['n_bottleneck']])),
                    'dec_w1': tf.Variable(tf.random_normal([layer_size['n_bottleneck'], layer_size['n_hidden']])),
                    'dec_w2': tf.Variable(tf.random_normal([layer_size['n_hidden'], layer_size['n_input']])),
                    }
        self.biases  = {'enc_b1': tf.Variable(tf.random_normal([layer_size['n_hidden']])),
                   'enc_b2': tf.Variable(tf.random_normal([layer_size['n_bottleneck']])),
                   'dec_b1': tf.Variable(tf.random_normal([layer_size['n_hidden']])),
                   'dec_b2': tf.Variable(tf.random_normal([layer_size['n_input']]))
                   }
        activation_func = tf.nn.sigmoid
        enc = self.encoder(x, self.weights, self.biases, activation_func)
        dec = self.decoder(enc, self.weights, self.biases, activation_func)
        return enc, dec

    def encoder(self, x, weights, biases, activation_func):
        layer_1 = activation_func(tf.add(tf.matmul(x, weights['enc_w1']), biases['enc_b1']))
        # layer_2 = activation_func(tf.add(tf.matmul(layer_1, weights['enc_w2']), biases['enc_b2']))
        layer_2 = tf.add(tf.matmul(layer_1, weights['enc_w2']), biases['enc_b2'])
        return layer_2

    def decoder(self, bottleneck, weights, biases, activation_func):
        layer_1 = activation_func(tf.add(tf.matmul(bottleneck, weights['dec_w1']), biases['dec_b1']))
        layer_2 = tf.add(tf.matmul(layer_1, weights['dec_w2']), biases['dec_b2'])
        return layer_2

    def l1_reg(self, params):
        return 0.1 * tf.reduce_sum(tf.abs(params))

    def loss_x_entropy(self, output, target):
        """Cross entropy loss

        See https://en.wikipedia.org/wiki/Cross_entropy

        Args:
          output: tensor of net output
          target: tensor of net we are trying to reconstruct
        Returns:
          Scalar tensor of cross entropy
        """
        with tf.name_scope("xentropy_loss"):
            net_output_tf = tf.convert_to_tensor(output, name='input')
            target_tf = tf.convert_to_tensor(target, name='target')
            cross_entropy = tf.add(tf.mul(tf.log(net_output_tf, name='log_output'),target_tf),tf.mul(tf.log(1 - net_output_tf),(1 - target_tf)))
            return -1 * tf.reduce_mean(tf.reduce_sum(cross_entropy, 1), name='xentropy_mean')

    def max_activations(self, X):
        w1, w2 = self.sess.run([self.weights['enc_w1'], self.weights['enc_w2']], feed_dict={self.X: X})
        w12 = np.dot(w1,w2)
        #return w12 / np.linalg.norm(w12)
        return w1 / np.linalg.norm(w1)

