from sklearn import linear_model

class ots_Logreg:

    def __init__(self):
        self.clf = linear_model.LogisticRegression()

    def fit(self, X, y):
        self.clf.fit(X, y)

    def predict(self, X):
        return self.clf.predict(X)

    def predict_log_proba(self, X):
        return self.clf.predict_log_proba(X)

