import numpy as np
import tensorflow as tf


class RecurrentNN:

    def __init__(self):
        pass

    def fit(self, X_train, y_train):
        # Parameters
        learning_rate = 0.01
        training_epochs = 20
        batch_size = 100
        display_step = 1

        # Network Parameters
        layer_size = {'n_input'  : 28,
                      'n_steps'  : 28,
                      'n_hidden' : 256,
                      'n_classes': 10
                      }

        X_train = X_train.reshape(-1, 28, 28)

        # Network input / ouput placeholders
        self.x = tf.placeholder(tf.float32, [None, layer_size['n_steps'], layer_size['n_input']])
        self.y = tf.placeholder("float", [None, layer_size['n_classes']])

        # Network structure, cost, and optmization method
        self.rnn = self.build_network(self.x, layer_size)
        cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(self.rnn, self.y))
        optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)

        # Create session
        init = tf.initialize_all_variables()
        self.sess = tf.Session()
        self.sess.run(init)

        # Training loop
        for epoch in range(training_epochs):
            print "Epoch " + str(epoch) + "..."
            avg_cost = 0
            total_batch = int(y_train.shape[0] / float(batch_size))
            for i in range(total_batch):
                batch_x = X_train[batch_size*i:batch_size*(i+1),:]
                batch_y = y_train[batch_size*i:batch_size*(i+1),:]
                _, c = self.sess.run([optimizer, cost], feed_dict={self.x: batch_x, self.y: batch_y})
                avg_cost += c / total_batch
                #if epoch % display_step == 0:
                    #print "Epoch:", '%04d' % (epoch + 1), "cost={:.9f}".format(avg_cost)
        print "Optimization Finished!"

    def predict(self, X_test):
        X_test = X_test.reshape(-1,28,28)
        return self.sess.run(tf.argmax(self.rnn, 1), feed_dict={self.x: X_test})

    def build_network(self, x, layer_size):
        weights = {'out': tf.Variable(tf.random_normal([layer_size['n_hidden'], layer_size['n_classes']]))}
        biases  = {'out': tf.Variable(tf.random_normal([layer_size['n_classes']]))}

        # Permuting batch_size and n_steps
        x = tf.transpose(x, [1, 0, 2])
        # Reshaping to (n_steps*batch_size, n_input)
        x = tf.reshape(x, [-1, layer_size['n_input']])
        # Split to get a list of 'n_steps' tensors of shape (batch_size, n_input)
        x = tf.split(0, layer_size['n_steps'], x)

        # Define a lstm cell with tensorflow
        lstm_cell = tf.nn.rnn_cell.BasicLSTMCell(layer_size['n_hidden'], forget_bias=1.0, state_is_tuple=True)

        # Get lstm cell output
        outputs, states = tf.nn.rnn(lstm_cell, x, dtype=tf.float32)

        # Linear activation, using rnn inner loop last output
        return tf.matmul(outputs[-1], weights['out']) + biases['out']


