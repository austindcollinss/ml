import numpy as np

class ots_Linreg:

    def __init__(self):
        self.coeffs = None
        pass

    def fit(self, X, y):
        X = np.hstack([X, np.ones((X.shape[0],1))])
        self.coeffs, residuals, rank, s = np.linalg.lstsq(X, y)

    def predict(self, X):
        X = np.hstack([X, np.ones((X.shape[0],1))])
        linreg_raw_y_hat = np.dot(X, self.coeffs)
        linreg_y_hat = np.round(linreg_raw_y_hat).astype(int)
        return linreg_y_hat

